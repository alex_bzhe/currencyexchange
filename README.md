### About

This is a test project developed with Spring Boot, Spring Data JPA and H2 database, representing a simple currency exchange REST service.

See the test task details and Swagger2 specifications in the respective passages below.

It's easy to convert swagger specification to human readable format with [bootprint-swagger](https://bootprint.knappi.org/).

### Deploy instructions

1. H2 in-memory database used. 
2. Build (with Maven) currencyexchange.war file and put it to 'webapps' folder of running Apache Tomcat.    
3. Or instead of currencyexchange.war build currencyexchange.jar with embedded Apache Tomcat and just run it on any JVM. To do it add [embedded Apache Tomcat dependency](https://mvnrepository.com/artifact/org.apache.tomcat.embed/tomcat-embed-core/9.0.34) to pom.xml and change there 'packaging' tag from war to jar.
4. After deploy, REST endpoints are to be available on localhost:8080/currencyexchange/api/...
5. Swagger docs are to be available on localhost:8080/currencyexchange/v2/api-docs and localhost:8080/currencyexchange/swagger-ui.html
6. Basic authentication required:
login: admin@admin.com, password: admin;
login: user@user.com, password: user;
login: customer@customer.com, password: customer.

### Details

Нужно реализовать сервис обмена валют на основании спецификации API. Требования по используемым технологиям: Spring Boot, можно использовать любую SQL базу данных. Регистрацию делать не нужно, достаточно нескольких сконфигурированных пользователей.

Выполненное задание должно содержать код и инструкцию по развертыванию. Для разворачивания инфраструктуры (БД) желательно использовать docker. Тесты по желанию.

 Некоторые примеры результатов работы сервиса:


    exchange rate        commission percent        amountFrom      amountTo
        1.000               3.00                    100.00          97.00
        0.500               15.00                   1.00            0.42
        5                   10                      25.25           113.62

'GIVE' operationType says the service to take amount from 'amountFrom', compute with appropriate currency pair's rate and commission and to write the result to 'amountTo'. And vice versa when 'operationType' is 'GET'.

### Swagger2 specifications

{
  "swagger": "2.0",
  "info": {
    "description": "Сервис обмена валют. Позволяет задавать курсы валют и комиссию за обмен по каждой валютной паре. На основании этих данных сервис позволяет получать суммы для прямого и обратного обмена валют.",
    "version": "1.0",
    "title": "Exchange service",
    "contact": {
      "email": "s.vasnev@advcash.com"
    }
  },
  "host": "localhost:8080",
  "basePath": "/",
  "tags": [
    {
      "name": "commissions",
      "description": "Значения комиссий взимаемых при обмене валют. Значение процента комиссии можно задать для кажой влаютной пары. Валидные значения в диапазоне от 0.00 до 100.00."
    },
    {
      "name": "exchange",
      "description": "Обмен валют. Позволяет получать информацию по суммам при прямом и обратном обмене валют с учетом комисии. Пример прямого обмена: обменять 100 USD на EUR, в этом случае запрос должен соержать объект вида: {\"amountFrom\": 100.00,\"currencyFrom\": \"USD\",\"currencyTo\": \"EUR\",\"operationType\":\"GIVE\"}. В ответ должен вернуться полностью заполненый объект. Пример обратного обмена: узнать сколько нужно USD для того чтобы получить в результате обмена 100 EUR, в этом случае запрос должен содержать объект вида: {\"amountTo\": 100.00,\"currencyFrom\": \"USD\",\"currencyTo\": \"EUR\",\"operationType\":\"GET\"}"
    },
    {
      "name": "exchange-rates",
      "description": "Значение курсов обмена валют. Ползволяют устанавлаивать и получать список курсов обмена валют для всех валютных пар. При установке курса обмена по любой из пар обратный курс должен быть установлен автоматически."
    }
  ],
  "securityDefinitions": {
    "basicAuth": {
      "type": "basic"
    }
  },
  "security": [
    {
      "basicAuth": []
    }
  ],
  "paths": {
    "/api/commissions": {
      "get": {
        "tags": [
          "commissions"
        ],
        "summary": "Получить список установленных комиссий",
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/Commission"
              }
            }
          },
          "401": {
            "description": "Unauthorized"
          }
        },
        "deprecated": false
      },
      "post": {
        "tags": [
          "commissions"
        ],
        "summary": "Установить значение комиссии для валютной пары",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "commission",
            "description": "commission",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Commission"
            }
          }
        ],
        "responses": {
          "201": {
            "description": "Created",
            "schema": {
              "$ref": "#/definitions/Commission"
            }
          },
          "400": {
            "description": "Error",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          },
          "401": {
            "description": "Unauthorized"
          }
        },
        "deprecated": false
      }
    },
    "/api/exchange": {
      "post": {
        "tags": [
          "exchange"
        ],
        "summary": "Запрос обмена валют",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "*/*"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "exchangeRequest",
            "description": "exchangeRequest",
            "required": true,
            "schema": {
              "$ref": "#/definitions/ExchangeRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/ExchangeRequest"
            }
          },
          "400": {
            "description": "Error",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          },
          "401": {
            "description": "Unauthorized"
          }
        },
        "deprecated": false
      }
    },
    "/api/exchange-rates": {
      "get": {
        "tags": [
          "exchange-rates"
        ],
        "summary": "Получить все курсы обмена валют",
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/ExchangeRate"
              }
            }
          },
          "401": {
            "description": "Unauthorized"
          }
        },
        "deprecated": false
      },
      "post": {
        "tags": [
          "exchange-rates"
        ],
        "summary": "Установить курс обмена валют по валютной паре. Курс обртаной пары жолжен быть установлен автоматически.",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "exchangeRate",
            "description": "exchangeRate",
            "required": true,
            "schema": {
              "$ref": "#/definitions/ExchangeRate"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/ExchangeRate"
            }
          },
          "400": {
            "description": "Error",
            "schema": {
              "$ref": "#/definitions/Error"
            }
          },
          "401": {
            "description": "Unauthorized"
          }
        },
        "deprecated": false
      }
    }
  },
  "definitions": {
    "Error": {
      "type": "object",
      "properties": {
        "description": {
          "type": "string"
        }
      }
    },
    "Commission": {
      "type": "object",
      "properties": {
        "commissionPt": {
          "type": "number"
        },
        "from": {
          "type": "string",
          "enum": [
            "EUR",
            "USD",
            "UAH",
            "RUB"
          ]
        },
        "to": {
          "type": "string",
          "enum": [
            "EUR",
            "USD",
            "UAH",
            "RUB"
          ]
        }
      },
      "title": "Commission"
    },
    "ExchangeRate": {
      "type": "object",
      "properties": {
        "from": {
          "type": "string",
          "enum": [
            "EUR",
            "USD",
            "UAH",
            "RUB"
          ]
        },
        "rate": {
          "type": "number"
        },
        "to": {
          "type": "string",
          "enum": [
            "EUR",
            "USD",
            "UAH",
            "RUB"
          ]
        }
      },
      "title": "ExchangeRate"
    },
    "ExchangeRequest": {
      "type": "object",
      "properties": {
        "amountFrom": {
          "type": "number"
        },
        "amountTo": {
          "type": "number"
        },
        "currencyFrom": {
          "type": "string",
          "enum": [
            "EUR",
            "USD",
            "UAH",
            "RUB"
          ]
        },
        "currencyTo": {
          "type": "string",
          "enum": [
            "EUR",
            "USD",
            "UAH",
            "RUB"
          ]
        },
        "operationType": {
          "type": "string",
          "enum": [
            "GET",
            "GIVE"
          ]
        }
      },
      "title": "ExchangeRequest"
    }
  }
}