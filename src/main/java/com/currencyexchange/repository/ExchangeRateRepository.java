package com.currencyexchange.repository;

import com.currencyexchange.model.CurrencyPair;
import com.currencyexchange.model.ExchangeRate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExchangeRateRepository extends JpaRepository<ExchangeRate, CurrencyPair> {
}