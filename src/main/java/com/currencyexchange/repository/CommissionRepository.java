package com.currencyexchange.repository;

import com.currencyexchange.model.Commission;
import com.currencyexchange.model.CurrencyPair;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommissionRepository extends JpaRepository<Commission, CurrencyPair> {
}