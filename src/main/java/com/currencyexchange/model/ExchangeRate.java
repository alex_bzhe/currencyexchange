package com.currencyexchange.model;

import com.currencyexchange.enumeration.Currency;
import com.currencyexchange.validation.ValidCurrencyPair;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.validation.constraints.Positive;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "exchange_rate")
@IdClass(CurrencyPair.class)
@ValidCurrencyPair(from = "from", to = "to")
public class ExchangeRate {

    @Id
    @Column(name = "rate_from")
    @Enumerated(EnumType.STRING)
    private Currency from;

    @Positive
    private Double rate;

    @Id
    @Column(name = "rate_to")
    @Enumerated(EnumType.STRING)
    private Currency to;
}
