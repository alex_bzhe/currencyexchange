package com.currencyexchange.model;

import com.currencyexchange.enumeration.Currency;
import com.currencyexchange.validation.ValidCurrencyPair;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@IdClass(CurrencyPair.class)
@ValidCurrencyPair(from = "from", to = "to")
public class Commission {

    @Column(name = "commission_pt")
    @DecimalMin(value = "0.00")
    @DecimalMax(value = "100.00")
    private Double commissionPt;

    @Id
    @Column(name = "commission_from")
    @Enumerated(EnumType.STRING)
    private Currency from;

    @Id
    @Column(name = "commission_to")
    @Enumerated(EnumType.STRING)
    private Currency to;
}