package com.currencyexchange.model;

import com.currencyexchange.enumeration.Currency;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CurrencyPair implements Serializable {

    private Currency from;
    private Currency to;
}
