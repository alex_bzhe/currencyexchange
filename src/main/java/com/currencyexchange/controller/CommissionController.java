package com.currencyexchange.controller;

import com.currencyexchange.model.Commission;
import com.currencyexchange.model.ExchangeRate;
import com.currencyexchange.service.CommissionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/commissions")
@RequiredArgsConstructor
@Api(tags = "commissions", description = "Commission values charged during currency exchange. " +
        "Allows to specify commission value for each currency pair. " +
        "Valid values are from 0.00 to 100.0.")
public class CommissionController {

    private final CommissionService commissionService;

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get list of all commissions for currency pairs")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = Commission.class),
            @ApiResponse(code = 401, message = "Unauthorized")})
    public List<Commission> getAllCommissions() {
        return commissionService.findAll();
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Set currency pair exchange rate. " +
            "The reverse currency pair exchange rate must be set automatically.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ExchangeRate.class),
            @ApiResponse(code = 400, message = "Error", response = com.currencyexchange.dto.Error.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden")})
    public Commission createCommission(@RequestBody @Valid Commission commission) {
        return commissionService.save(commission);
    }
}