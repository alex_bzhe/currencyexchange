package com.currencyexchange.controller;

import com.currencyexchange.dto.ExchangeRequest;
import com.currencyexchange.model.ExchangeRate;
import com.currencyexchange.service.ExchangeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/exchange")
@RequiredArgsConstructor
@Api(tags = "exchange", description = "Currency exchange. " +
        "Allows to get info about amounts of direct and reverse exchange. " +
        "Direct exchange sample: " +
        "to exchange 100 USD for EUR, the request should contain an object like: " +
        "{\"amountFrom\": 100.00,\"currencyFrom\": \"USD\",\"currencyTo\": \"EUR\",\"operationType\":\"GIVE\"}. " +
        "The response should return completely fulfilled object. " +
        "Reverse exchange sample: to find how much USD needed to get 100 EUR, " +
        "in this cse the request should contain an object like: " +
        "{\"amountTo\": 100.00,\"currencyFrom\": \"USD\",\"currencyTo\": \"EUR\",\"operationType\":\"GET\"}")
public class ExchangeController {

    private final ExchangeService exchangeService;

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Currency exchange request")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ExchangeRate.class),
            @ApiResponse(code = 400, message = "Error", response = com.currencyexchange.dto.Error.class),
            @ApiResponse(code = 401, message = "Unauthorized")})
    public ExchangeRequest exchange(@RequestBody @Valid ExchangeRequest exchangeRequest) {
        return exchangeService.exchange(exchangeRequest);
    }
}