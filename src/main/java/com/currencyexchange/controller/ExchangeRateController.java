package com.currencyexchange.controller;

import com.currencyexchange.model.ExchangeRate;
import com.currencyexchange.service.ExchangeRateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/exchange-rates")
@RequiredArgsConstructor
@Api(tags = "exchange-rates", description = "Currency exchange rates values. " +
        "Allows to set exchange rate for a currency pair and to get list of " +
        "all currency pairs exchange rates. When one currency pair exchange rate " +
        "is set, the reverse currency pair exchange rate must be set automatically.")
public class ExchangeRateController {

    private final ExchangeRateService exchangeRateService;

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get list of all currency pairs exchange rates")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ExchangeRate.class),
            @ApiResponse(code = 401, message = "Unauthorized")})
    public List<ExchangeRate> getAllExchangeRates() {
        return exchangeRateService.findAll();
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Set currency pair exchange rate. " +
            "The reverse currency pair exchange rate must be set automatically.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ExchangeRate.class),
            @ApiResponse(code = 400, message = "Error", response = com.currencyexchange.dto.Error.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden")})
    public ExchangeRate createExchangeRate(@RequestBody @Valid ExchangeRate exchangeRate) {
        return exchangeRateService.saveStraightAndReverseExchangeRates(exchangeRate);
    }
}