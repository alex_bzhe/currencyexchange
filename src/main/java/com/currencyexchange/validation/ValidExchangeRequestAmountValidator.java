package com.currencyexchange.validation;

import com.currencyexchange.dto.ExchangeRequest;
import com.currencyexchange.enumeration.OperationType;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ValidExchangeRequestAmountValidator implements ConstraintValidator<ValidExchangeRequestAmount, ExchangeRequest> {

    @Override
    public boolean isValid(final ExchangeRequest value, final ConstraintValidatorContext context) {
        try {
            final OperationType operationType = value.getOperationType();
            final Double amountFrom = value.getAmountFrom();
            final Double amountTo = value.getAmountTo();
            return isValidAmount(amountFrom, amountTo, operationType);
        } catch (final Exception ignored) {
            return false;
        }
    }

    private boolean isValidAmount(final Double amountFrom, final Double amountTo, final OperationType operationType) {
        final Double amountToValidate = operationType.equals(OperationType.GIVE) ? amountFrom : amountTo;
        return amountToValidate != null && amountToValidate > 0;
    }
}