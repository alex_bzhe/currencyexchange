package com.currencyexchange.validation;

import com.currencyexchange.enumeration.Currency;
import org.apache.commons.beanutils.BeanUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ValidCurrencyPairValidator implements ConstraintValidator<ValidCurrencyPair, Object> {

    private String from;
    private String to;

    @Override
    public void initialize(final ValidCurrencyPair constraintAnnotation) {
        from = constraintAnnotation.from();
        to = constraintAnnotation.to();
    }

    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context) {
        try {
            final Currency firstCurrency = Currency.valueOf(BeanUtils.getProperty(value, from));
            final Currency secondCurrency = Currency.valueOf(BeanUtils.getProperty(value, to));
            return !firstCurrency.equals(secondCurrency);
        } catch (final Exception ignored) {
            return false;
        }
    }
}