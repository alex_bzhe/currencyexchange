package com.currencyexchange.service;

import com.currencyexchange.model.CurrencyPair;
import com.currencyexchange.model.ExchangeRate;

import java.util.List;
import java.util.Optional;

public interface ExchangeRateService {

    List<ExchangeRate> findAll();

    ExchangeRate saveStraightAndReverseExchangeRates(ExchangeRate exchangeRate);

    ExchangeRate findById(CurrencyPair currencyPair);
}
