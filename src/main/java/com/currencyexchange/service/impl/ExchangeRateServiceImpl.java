package com.currencyexchange.service.impl;

import com.currencyexchange.exception.ExchangeRateNotFoundException;
import com.currencyexchange.model.CurrencyPair;
import com.currencyexchange.model.ExchangeRate;
import com.currencyexchange.repository.ExchangeRateRepository;
import com.currencyexchange.service.ExchangeRateService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class ExchangeRateServiceImpl implements ExchangeRateService {

    private final ExchangeRateRepository exchangeRateRepository;

    @Override
    public List<ExchangeRate> findAll() {
        return exchangeRateRepository.findAll();
    }

    @Override
    public ExchangeRate saveStraightAndReverseExchangeRates(ExchangeRate exchangeRate) {
        final ExchangeRate reverseExchangeRate = new ExchangeRate();
        reverseExchangeRate.setFrom(exchangeRate.getTo());
        reverseExchangeRate.setTo(exchangeRate.getFrom());
        reverseExchangeRate.setRate(1 / exchangeRate.getRate());
        exchangeRateRepository.save(reverseExchangeRate);
        return exchangeRateRepository.save(exchangeRate);
    }

    @Override
    public ExchangeRate findById(CurrencyPair currencyPair) {
        return exchangeRateRepository.findById(currencyPair).orElseThrow(ExchangeRateNotFoundException::new);
    }
}
