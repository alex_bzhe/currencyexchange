package com.currencyexchange.service.impl;

import com.currencyexchange.exception.CommissionNotFoundException;
import com.currencyexchange.model.Commission;
import com.currencyexchange.model.CurrencyPair;
import com.currencyexchange.repository.CommissionRepository;
import com.currencyexchange.service.CommissionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class CommissionServiceImpl implements CommissionService {

    private final CommissionRepository commissionRepository;

    @Override
    public List<Commission> findAll() {
        return commissionRepository.findAll();
    }

    @Override
    public Commission save(Commission commission) {
        return commissionRepository.save(commission);
    }

    @Override
    public Commission findById(CurrencyPair currencyPair) {
        return commissionRepository.findById(currencyPair).orElseThrow(CommissionNotFoundException::new);
    }
}
