package com.currencyexchange.service.impl;

import com.currencyexchange.dto.ExchangeRequest;
import com.currencyexchange.enumeration.OperationType;
import com.currencyexchange.model.CurrencyPair;
import com.currencyexchange.service.CommissionService;
import com.currencyexchange.service.ExchangeRateService;
import com.currencyexchange.service.ExchangeService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.math3.util.Precision;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import static java.math.BigDecimal.ROUND_DOWN;
import static java.math.BigDecimal.ROUND_UP;

@Service
@Transactional
@RequiredArgsConstructor
public class ExchangeServiceImpl implements ExchangeService {

    private final CommissionService commissionService;
    private final ExchangeRateService exchangeRateService;

    @Override
    public ExchangeRequest exchange(final ExchangeRequest exchangeRequest) {
        final CurrencyPair currencyPair =
                new CurrencyPair(exchangeRequest.getCurrencyFrom(), exchangeRequest.getCurrencyTo());
        final double commissionPt = commissionService.findById(currencyPair).getCommissionPt();
        final double rate = exchangeRateService.findById(currencyPair).getRate();
        final boolean straight = getStraight(exchangeRequest);
        final boolean roundDown = getRoundDown(straight);
        final double knownAmount = getKnownAmount(exchangeRequest, straight);
        final double requestedAmount = getRequestedAmount(knownAmount, commissionPt, rate, roundDown, straight);
        return getExchangeResponse(requestedAmount, straight, exchangeRequest);
    }

    private ExchangeRequest getExchangeResponse(double requestedAmount, boolean straight, ExchangeRequest exchangeRequest) {
        if (straight) {
            exchangeRequest.setAmountTo(requestedAmount);
        } else {
            exchangeRequest.setAmountFrom(requestedAmount);
        }
        return exchangeRequest;
    }

    private double getRequestedAmount(double knownAmount, double commissionPt,
                                      double rate, boolean roundDown, boolean straight) {
        final double requestedAmount;
        if (straight) {
            requestedAmount = knownAmount * (100 - commissionPt) * rate / 100;
        } else {
            requestedAmount = knownAmount / (100 - commissionPt) / rate * 100;
        }
        return roundAmount(requestedAmount, roundDown);
    }

    private double roundAmount(double amount, boolean roundDown) {
        return Precision.round(amount, 2, roundDown ? ROUND_DOWN : ROUND_UP);
    }

    private double getKnownAmount(final ExchangeRequest exchangeRequest, boolean straight) {
        return straight ? exchangeRequest.getAmountFrom() : exchangeRequest.getAmountTo();
    }

    private boolean getRoundDown(boolean straight) {
        return straight;
    }

    private boolean getStraight(final ExchangeRequest exchangeRequest) {
        return exchangeRequest.getOperationType().equals(OperationType.GIVE);
    }
}
