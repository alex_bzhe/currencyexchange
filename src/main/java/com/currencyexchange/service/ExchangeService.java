package com.currencyexchange.service;

import com.currencyexchange.dto.ExchangeRequest;

public interface ExchangeService {
    ExchangeRequest exchange(ExchangeRequest exchangeRequest);
}
