package com.currencyexchange.service;

import com.currencyexchange.model.Commission;
import com.currencyexchange.model.CurrencyPair;

import java.util.List;
import java.util.Optional;

public interface CommissionService {

    List<Commission> findAll();

    Commission save(Commission commission);

    Commission findById(CurrencyPair currencyPair);
}
