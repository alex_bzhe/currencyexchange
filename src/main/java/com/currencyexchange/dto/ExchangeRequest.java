package com.currencyexchange.dto;

import com.currencyexchange.enumeration.Currency;
import com.currencyexchange.enumeration.OperationType;
import com.currencyexchange.validation.ValidCurrencyPair;
import com.currencyexchange.validation.ValidExchangeRequestAmount;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ValidCurrencyPair(from = "currencyFrom", to = "currencyTo")
@ValidExchangeRequestAmount
public class ExchangeRequest {

    Double amountFrom;
    Double amountTo;
    Currency currencyFrom;
    Currency currencyTo;
    OperationType operationType;
}