package com.currencyexchange.enumeration;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum OperationType {
    GIVE("GIVE"),
    GET("GET");

    private final String value;
}
