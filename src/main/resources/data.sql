-- --------------------------------------------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (

    `id` 				BIGINT(20) NOT NULL AUTO_INCREMENT,
    `email`		 		VARCHAR(50) NOT NULL UNIQUE,
    `password` 			VARCHAR(255) NOT NULL,

    CONSTRAINT `user_pk` PRIMARY KEY(`id`),
    INDEX `email_index`(`email`)
);
-- --------------------------------------------------------------
INSERT INTO `user` VALUES(DEFAULT, 'admin@admin.com', '$2a$10$.PnnLwr04XIovgiiYpDZk.qXrkXMfx83AvDudSjMCmsBDaUHILAy6');
INSERT INTO `user` VALUES(DEFAULT, 'user@user.com', '$2a$10$xOPzBDZ3R1f19pUOPXjy8e10IDKRnc6jQl1keX6IQJI6L75cJBxZS');
INSERT INTO `user` VALUES(DEFAULT, 'customer@customer.com', '$2a$10$TeWeNx3keDApvV8YWTlVY.4gREjwY8fz1WFbuiLwQUAgtLc14lvVm');
-- --------------------------------------------------------------
-- --------------------------------------------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (

  `user_id`                 BIGINT(20) NOT NULL,
  `user_role_string`        VARCHAR(50) NOT NULL,

  CONSTRAINT `user_role_pk` PRIMARY KEY(`user_id`, `user_role_string`),
  CONSTRAINT `user_id_fk` FOREIGN KEY(`user_id`)
      REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
);
-- --------------------------------------------------------------
INSERT INTO `user_role` VALUES(1, 'ADMIN');
INSERT INTO `user_role` VALUES(2, 'USER');
INSERT INTO `user_role` VALUES(3, 'CUSTOMER');
-- --------------------------------------------------------------
-- --------------------------------------------------------------
DROP TABLE IF EXISTS `commission`;
CREATE TABLE `commission` (

  `commission_pt`                      DECIMAL(5,2) NOT NULL,
  `commission_from`                    VARCHAR(3) NOT NULL,
  `commission_to`                      VARCHAR(3) NOT NULL,

  CONSTRAINT `commission_pk` PRIMARY KEY(`commission_from`, `commission_to`)
);
-- --------------------------------------------------------------
INSERT INTO `commission` VALUES(3, 'USD', 'EUR');
INSERT INTO `commission` VALUES(3, 'EUR', 'USD');
INSERT INTO `commission` VALUES(15, 'RUB', 'UAH');
INSERT INTO `commission` VALUES(15, 'UAH', 'RUB');
INSERT INTO `commission` VALUES(10, 'USD', 'UAH');
INSERT INTO `commission` VALUES(10, 'UAH', 'USD');
-- --------------------------------------------------------------
-- --------------------------------------------------------------
DROP TABLE IF EXISTS `exchange_rate`;
CREATE TABLE `exchange_rate` (

  `rate_from`                    VARCHAR(3) NOT NULL,
  `rate`                         DECIMAL(40,20) NOT NULL,
  `rate_to`                      VARCHAR(3) NOT NULL,

  CONSTRAINT `exchange_rate_pk` PRIMARY KEY(`rate_from`, `rate_to`)
);
-- --------------------------------------------------------------
INSERT INTO `exchange_rate` VALUES('USD', 1, 'EUR');
INSERT INTO `exchange_rate` VALUES('EUR', 1, 'USD');
INSERT INTO `exchange_rate` VALUES('RUB', 0.5, 'UAH');
INSERT INTO `exchange_rate` VALUES('UAH', 2, 'RUB');
INSERT INTO `exchange_rate` VALUES('USD', 5, 'UAH');
INSERT INTO `exchange_rate` VALUES('UAH', 0.2, 'USD');
-- --------------------------------------------------------------