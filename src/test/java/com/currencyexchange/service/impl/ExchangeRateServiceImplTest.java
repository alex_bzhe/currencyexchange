package com.currencyexchange.service.impl;

import com.currencyexchange.enumeration.Currency;
import com.currencyexchange.exception.ExchangeRateNotFoundException;
import com.currencyexchange.model.CurrencyPair;
import com.currencyexchange.model.ExchangeRate;
import com.currencyexchange.repository.ExchangeRateRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

class ExchangeRateServiceImplTest {

    @Mock
    private ExchangeRateRepository exchangeRateRepository;

    @Captor
    private ArgumentCaptor<ExchangeRate> exchangeRateCaptor;

    @InjectMocks
    private ExchangeRateServiceImpl exchangeRateService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenCanNotFindById_thenShouldThrowExchangeRateNotFoundException() {
        when(exchangeRateRepository.findById(any(CurrencyPair.class))).thenReturn(Optional.empty());

        assertThrows(ExchangeRateNotFoundException.class, () -> exchangeRateService.findById(new CurrencyPair()));
    }

    @Test
    public void whenSaveStraightAndReverseExchangeRates_thenShouldCreateCorrectlyReversedExchangeRateAndSaveBoth() {
        final ExchangeRate straightRate = createExchangeRate();

        exchangeRateService.saveStraightAndReverseExchangeRates(straightRate);

        verify(exchangeRateRepository, times(2)).save(exchangeRateCaptor.capture());
        final ExchangeRate savedReversedRate = exchangeRateCaptor.getAllValues().get(0);
        final ExchangeRate savedStraightRate = exchangeRateCaptor.getAllValues().get(1);
        assertTrue(isExchangeRateCorrectlyReversed(straightRate, savedReversedRate));
        assertSame(straightRate, savedStraightRate);
    }

    private ExchangeRate createExchangeRate() {
        final ExchangeRate straightRate = new ExchangeRate();
        straightRate.setFrom(Currency.EUR);
        straightRate.setTo(Currency.RUB);
        straightRate.setRate(2.0);
        return straightRate;
    }

    private boolean isExchangeRateCorrectlyReversed(final ExchangeRate source, final ExchangeRate target) {
        return source.getFrom().equals(target.getTo())
                && source.getTo().equals(target.getFrom())
                && source.getRate().equals(1 / target.getRate());
    }
}