package com.currencyexchange.service.impl;

import com.currencyexchange.exception.CommissionNotFoundException;
import com.currencyexchange.model.CurrencyPair;
import com.currencyexchange.repository.CommissionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class CommissionServiceImplTest {

    @Mock
    private CommissionRepository commissionRepository;

    @InjectMocks
    private CommissionServiceImpl commissionService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whenCanNotFindById_thenShouldThrowCommissionNotFoundException() {
        when(commissionRepository.findById(any(CurrencyPair.class))).thenReturn(Optional.empty());

        assertThrows(CommissionNotFoundException.class, () -> commissionService.findById(new CurrencyPair()));
    }
}