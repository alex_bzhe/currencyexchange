package com.currencyexchange.service.impl;

import com.currencyexchange.dto.ExchangeRequest;
import com.currencyexchange.enumeration.Currency;
import com.currencyexchange.enumeration.OperationType;
import com.currencyexchange.model.Commission;
import com.currencyexchange.model.CurrencyPair;
import com.currencyexchange.model.ExchangeRate;
import com.currencyexchange.service.CommissionService;
import com.currencyexchange.service.ExchangeRateService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class ExchangeServiceImplTest {

    private final CurrencyPair currencyPair = new CurrencyPair(Currency.USD, Currency.UAH);

    @Mock
    private ExchangeRateService exchangeRateService;

    @Mock
    private CommissionService commissionService;

    @Captor
    private ArgumentCaptor<CurrencyPair> currencyPairCaptor;

    @InjectMocks
    private ExchangeServiceImpl exchangeService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void whenExchange_thenShouldCallExchangeRateAndCommissionServicesWithRightCurrencyPairArgument() {
        when(commissionService.findById(any(CurrencyPair.class))).thenReturn(createCommission());
        when(exchangeRateService.findById(any(CurrencyPair.class))).thenReturn(createExchangeRate());

        exchangeService.exchange(createExchangeRequest());

        verify(commissionService).findById(currencyPairCaptor.capture());
        verify(exchangeRateService).findById(currencyPairCaptor.capture());
        final CurrencyPair commissionCurrencyPair = currencyPairCaptor.getAllValues().get(0);
        final CurrencyPair exchangeRateCurrencyPair = currencyPairCaptor.getAllValues().get(1);
        assertSame(commissionCurrencyPair, exchangeRateCurrencyPair);
        assertEquals(commissionCurrencyPair, currencyPair);
    }

    private ExchangeRate createExchangeRate() {
        ExchangeRate exchangeRate = new ExchangeRate();
        exchangeRate.setRate(27.5);
        exchangeRate.setFrom(currencyPair.getFrom());
        exchangeRate.setTo(currencyPair.getTo());
        return exchangeRate;
    }

    private Commission createCommission() {
        Commission commission = new Commission();
        commission.setCommissionPt(10.0);
        commission.setFrom(currencyPair.getFrom());
        commission.setTo(currencyPair.getTo());
        return commission;
    }

    private ExchangeRequest createExchangeRequest() {
        final ExchangeRequest exchangeRequest = new ExchangeRequest();
        exchangeRequest.setAmountFrom(100.0);
        exchangeRequest.setAmountTo(null);
        exchangeRequest.setCurrencyFrom(currencyPair.getFrom());
        exchangeRequest.setCurrencyTo(currencyPair.getTo());
        exchangeRequest.setOperationType(OperationType.GIVE);
        return exchangeRequest;
    }
}